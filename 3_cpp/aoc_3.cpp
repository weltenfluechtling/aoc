#include <iostream>
#include <string>
#include <fstream>

struct Claim {

    size_t id;
    size_t left_margin;
    size_t top_margin;
    size_t width;
    size_t height;

    Claim(const std::string& line) {
        for (std::string::const_iterator it = line.begin(); it != line.end(); ++it) {
            if (*it == '#') {
                ++it;
                std::string id = "";
                while (*it != ' ') {
                    id += *(it++);
                }
                this->id = std::stoi(id);
            }
            if (*it == '@') {
                it += 2;
                std::string left_margin = "";
                while (*it != ',') {
                    left_margin += *(it++);
                }
                this->left_margin = std::stoi(left_margin);
                ++it;
                std::string top_margin = "";
                while (*it != ':') {
                    top_margin += *(it++);
                }
                this->top_margin = std::stoi(top_margin);
            }
            if (*it == ':') {
                it += 2;
                std::string width = "";
                while (*it != 'x') {
                    width += *(it++);
                }
                this->width = std::stoi(width);
                ++it;
                std::string height = "";
                while (it != line.end()) {
                    height += *(it++);
                }
                this->height = std::stoi(height);
                --it;
            }
        }
    }

    int get_far_right_field_num() const {
        return this->left_margin + this->width;
    }

    int get_far_bottom_field_num() const {
        return this->top_margin + this->height;
    }

};

class Solve_Day_3 {
    
    static const size_t NUM_LINES = 1401;

    std::ifstream file;
    Claim** claims;
    size_t field_width;
    size_t field_height;
    int** field;

public:

    Solve_Day_3(): claims(new Claim*[NUM_LINES]()) {
        file.open("./aoc_3.input.txt");
        if (file.is_open()) {
            std::string line;
            int i = 0;
            int max_width = 0;
            int max_height = 0;
            while (std::getline(file, line)) {
                claims[i] = new Claim(line);
                if (claims[i]->get_far_right_field_num() > max_width) {
                    max_width = claims[i]->get_far_right_field_num();
                }
                if (claims[i]->get_far_bottom_field_num() > max_height) {
                    max_height = claims[i]->get_far_bottom_field_num();
                }
                ++i;
            }
            field_width = max_width;
            field_height = max_height;
            field = new int*[field_height];
            for (size_t i = 0; i < field_height; ++i) {
                field[i] = new int[field_width];
            }
            for (size_t i = 0; i < field_height; ++i) {
                for (size_t j = 0; j < field_width; ++j) {
                    field[i][j] = 0;
                }
            }
        }
    }

    Solve_Day_3(const Solve_Day_3& other) = delete;
    Solve_Day_3(Solve_Day_3&& other) = delete;
    Solve_Day_3& operator=(const Solve_Day_3& other) = delete;
    Solve_Day_3& operator=(Solve_Day_3&& other) = delete;

    virtual ~Solve_Day_3() {
        for (size_t i = NUM_LINES; i-- > 0;) {
            delete claims[i];
        }
        delete[] claims;
        for (size_t i = 0; i < field_height; ++i) {
            delete[] field[i];
        }
        delete[] field;
    }

    size_t solve_part_1() {
        size_t counter = 0;
        for (size_t i = 0; i < field_height; ++i) {
            for (size_t j = 0; j < field_width; ++j) {
                if (field[i][j] == -1) {
                    ++counter;
                }
            }
        }
        return counter;
    }

    size_t solve_part_2() {
        for (size_t i = NUM_LINES; i-- > 0;) {
            if (!claimIsOverwritten(*claims[i])) {
                return claims[i]->id;
            }
        }
        return 0;
    }

    std::ostream& print(std::ostream& o) const {
        for (size_t i = 0; i < field_height; ++i) {
            for (size_t j = 0; j < field_width; ++j) {
                o << field[j][i] << "|";
            }
            o << std::endl;
        }
        return o;
    }

    void populate_field() {
        for (size_t ci = NUM_LINES; ci-- > 0;) {
            for (size_t i = claims[ci]->left_margin; i < claims[ci]->left_margin + claims[ci]->width; ++i) {
                for (size_t j = claims[ci]->top_margin; j < claims[ci]->top_margin + claims[ci]->height; ++j) {
                    field[i][j] = field[i][j] ? -1 : claims[ci]->id;
                }
            }
        }
    }

private:

    bool claimIsOverwritten(const Claim& claim) const {
        for (size_t i = claim.left_margin; i < claim.left_margin + claim.width; ++i) {
            for (size_t j = claim.top_margin; j < claim.top_margin + claim.height; ++j) {
                if (field[i][j] == -1) {
                    return true;
                }
            }
        }
        return false;
    }

};

std::ostream& operator<<(std::ostream& o, const Solve_Day_3& s) {
    return s.print(o);
}

int main() {
    Solve_Day_3 s;
    s.populate_field();
    std::cout << "Number of overlapping claims: " << s.solve_part_1() << std::endl;
    std::cout << "Consensual claim: " << s.solve_part_2() << std::endl;
}