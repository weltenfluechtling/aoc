import java.io.BufferedReader;
import java.io.IOException;
import java.lang.System;
import java.nio.file.*;
import java.nio.charset.Charset;

class Solve_Day_5 {

    private Path path;
    private String data;

    public Solve_Day_5() {
        path = FileSystems.getDefault().getPath("./aoc_5.input.txt");
        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                data = line;
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public int solve_part_1() {
        return react();
    }

    public int solve_part_2() {
        StringBuilder sb = new StringBuilder();
        int bestResult = Integer.MAX_VALUE;
        for (char c = 'a'; c <= 'z'; ++c) {
            String data = removeLetter(c);
            int newResult = react(data);
            if (newResult < bestResult) {
                bestResult = newResult;
            }
        }
        return bestResult;
    }

    private int react() {
        return react(data);
    }

    private int react(final String data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length() - 1; ++i) {
            if (toBeDeleted(data.charAt(i), data.charAt(i + 1))) {
                i += 2;
                if (i < data.length()) {
                    sb.append(data.charAt(i));
                    checkIntegrity(sb);
                }
            } else {
                sb.append(data.charAt(i));
                checkIntegrity((sb));
            }
        }
        sb.append(data.charAt(data.length() - 1));
        return sb.length();
    }

    private void checkIntegrity(StringBuilder sb) {
        char last = sb.charAt(sb.length() - 1);
        if (sb.length() > 1) {
            char secondLast = sb.charAt(sb.length() - 2);
            if (toBeDeleted(last, secondLast)) {
                sb.delete(sb.length() - 2, sb.length());
            }
        }
    }

    private String removeLetter(final char c) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length(); ++i) {
            if (data.charAt(i) != c && data.charAt(i) != Character.toUpperCase(c)) {
                sb.append(data.charAt(i));
            }
        }
        return sb.toString();
    }

    /**
     * @return true, if the characters have to be deleted
     */
    private boolean toBeDeleted(final char a, final char b) {
        if (
                (Character.isUpperCase(a) && Character.isLowerCase(b)) ||
                (Character.isLowerCase(a) && Character.isUpperCase(b))
        ) {
            if (Character.toLowerCase(a) == Character.toLowerCase(b)) {
                return true;
            }
        }
        return false;
    }

}

public class Aoc_5 {
    public static void main(String[] args) {
        Solve_Day_5 s = new Solve_Day_5();
        System.out.println("Result for Part 1: " + s.solve_part_1());
        System.out.println("Result for Part 2: " + s.solve_part_2());
    }
}