class Solve_Day_2

    @@FILENAME = './aoc_2.input.txt'

    def initialize
        @numTwoTimes = 0
        @numThreeTimes = 0
        @lines = Array.new
        fh = open @@FILENAME
        while (line = fh.gets)
            @lines.push(line)
        end
        fh.close
    end

    def solve_part_1
        @lines.each do |line|
            found_double = false
            found_triple = false
            line.each_char do |char|
                num = line.count char
                if num === 2
                    found_double = true
                elsif num === 3
                    found_triple = true
                end
            end
            @numTwoTimes += 1 if found_double
            @numThreeTimes += 1 if found_triple
        end
        @numTwoTimes * @numThreeTimes
    end

    def solve_part_2
        @lines.each_with_index do |source, i|
            @lines.each_with_index do |comp, j|
                if (i != j)
                    if Solve_Day_2.compare source, comp
                        return Solve_Day_2.get_result_string source, comp
                    end
                end
            end
        end
    end

private

    def self.compare str1, str2
        diff = 0
        i = 0
        while (i != str1.length)
            if str1.slice(i) != str2.slice(i)
                diff += 1
                return false if diff >= 2
            end
            i += 1
        end
        true
    end

    def self.get_result_string str1, str2
        result = ""
        str1.each_char.with_index do |c, i|
            if c == str2.slice(i)
                result += c
            end
        end
        result
    end

end

res = Solve_Day_2.new
puts res.solve_part_1
puts res.solve_part_2
