import 'dart:io';
import '../models/location.dart';

Future<List<String>> readFile(String fileName) async {
	var config = File(fileName);
	return config.readAsLines();
}

Location createLocationByInputLine(String line) {
	List<String> splitStr = line.split(',');
	splitStr[1] = splitStr[1].trim();
	return Location(int.parse(splitStr[0]), int.parse(splitStr[1]));
}