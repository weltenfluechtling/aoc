import 'package:uuid/uuid.dart';

var uuid = Uuid();

class Location {

	final String id;
	bool isInfinite;
	final int x;
	final int y;

	Location(this.x, this.y): id = uuid.v4(), isInfinite = false;

	@override String toString() => '${id}: (${x}, ${y}), is infinite: $isInfinite';

}