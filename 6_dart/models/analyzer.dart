import 'grid.dart';
import 'location.dart';

class Analyzer {

	final Grid _grid;
	final List<Location> _locations;

	Analyzer(this._grid, this._locations);

	int solvePart1() {
		int highest = 0;
		_locations.forEach((l) {
			int occupiedFields = _grid.getNumberOfOccupiedFieldsForLocation(l);
			if (highest < occupiedFields) highest = occupiedFields;
		});
		return highest;
	}

	int solvePart2() {
		return _grid.getRegionSize(_locations);
	}
}