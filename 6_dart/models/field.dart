import 'location.dart';

class Field {

	final int x;
	final int y;
	Location nearest;
	int dNearest;
	bool hasMoreThanOneNearest;

	Field(this.x, this.y) : hasMoreThanOneNearest = false;

	@override String toString() {
		String addition = 'Nearest: no nearest';
		if (nearest != null) {
			addition = 'Nearest: $nearest, Distance: $dNearest';
		}
		return '($x, $y), more than one nearest : $hasMoreThanOneNearest ($addition)';
	}
}