import 'field.dart';
import 'location.dart';

class Grid {

	List<List<Field>> _fields;
	final int _highestX;
	final int _highestY;

	Grid(int highestX, int highestY) : _highestX = highestX, _highestY = highestY {
		_fields = List<List<Field>>.generate(
			_highestX + 1,
			(i) => List<Field>.generate(
				_highestY + 1, (j) => Field(i, j))
		);
	}

	Field getField(int x, int y) {
		return _fields[x][y];
	}

	void drawLocation(Location l) {
		for (var i = 0; i < _fields.length; ++i) {
			List<Field> vertical = _fields[i];
			for (var j = 0; j < vertical.length; ++j) {
				Field f = vertical[j];
				int bufDNearest = ((f.x - l.x).abs()) + ((f.y - l.y).abs());
				if (!f.hasMoreThanOneNearest) {
					if (f.nearest == null) {
						f.nearest = l;
						f.dNearest = bufDNearest;
					} else if (bufDNearest < f.dNearest) {
						f.nearest = l;
						f.dNearest = bufDNearest;
					} else if (bufDNearest == f.dNearest) {
						f.hasMoreThanOneNearest = true;
						f.nearest = null;
						f.dNearest = bufDNearest;
					}
				} else {
					if (bufDNearest < f.dNearest) {
						f.hasMoreThanOneNearest = false;
						f.nearest = l;
						f.dNearest = bufDNearest;
					}
				}
			}
		}
	}

	void markInifinites() {
		for (var i = 0; i <= _highestX; ++i) {
			List<Field> vertical = _fields[i];
			final int maxJ = (i == 0 || i == _highestX) ? _highestY + 1 : 1;
			for (var j = 0; j < maxJ; ++j) {
				Field f = vertical[j];
				if (f.nearest != null) f.nearest.isInfinite = true;
			}

			if (maxJ == 1) {
				Field f = vertical[_highestY];
				if (f.nearest != null) f.nearest.isInfinite = true;
			}
		}
	}

	int getNumberOfOccupiedFieldsForLocation(Location l) {
		int occupied = 0;
		for (var i = 0; i < _fields.length; ++i) {
			List<Field> vertical = _fields[i];
			for (var j = 0; j < vertical.length; ++j) {
				Field f = vertical[j];
				if (f.nearest != null && f.nearest.isInfinite == false && f.nearest.id == l.id) {
					++occupied;
				}
			}
		}
		return occupied;
	}

	void printGrid() {
		for (int y = 0; y <= _highestY; ++y) {
			for (int x = 0; x <= _highestX; ++x) {
				print('($x, $y): ${_fields[x][y]}\n');
			}
		}
	}

	int getRegionSize(List<Location> locations) {
		int regionSize = 0;
		for (var i = 0; i < _fields.length; ++i) {
			List<Field> vertical = _fields[i];
			for (var j = 0; j < vertical.length; ++j) {
				Field f = vertical[j];
				int regionValue = 0;
				locations.forEach((Location l) {
					regionValue += ((f.x - l.x).abs()) + ((f.y - l.y).abs());
				});
				if (regionValue < 10000) ++regionSize;
			}
		}
		return regionSize;
	}

}