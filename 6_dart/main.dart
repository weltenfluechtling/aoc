import 'util/functions.dart';
import 'models/location.dart';
import 'models/grid.dart';
import 'models/analyzer.dart';

main() async {
	List<String> content = await readFile('input.txt');
	List<Location> locations = List();

	int highestX = 0;
	int highestY = 0;

	content.forEach((str) {
		Location l = createLocationByInputLine(str);
		locations.add(l);
		if (l.x > highestX) highestX = l.x;
		if (l.y > highestY) highestY = l.y;
	});

	final Grid grid = Grid(highestX, highestY);
	locations.forEach((Location l) {
		grid.drawLocation(l);
	});

	grid.markInifinites();

	Analyzer analyzer = Analyzer(grid, locations);
	print('Solution 1: ${analyzer.solvePart1()}');
	print('Solution 2: ${analyzer.solvePart2()}');

}