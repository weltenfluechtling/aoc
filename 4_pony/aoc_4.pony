use "files"
use "time"
use "collections"

class Event
    let _time: PosixDate
    let _description: String

    new create(time: PosixDate, description': String) =>
        _time = time
        _description = description'

    fun string() : String => _time.format("%Y-%m-%d %H:%M") + " " + _description
    fun date() : PosixDate box => _time
    fun description() : String => _description

class Guard
    var _id: USize
    var _minutesAsleep: USize

    new create(id': USize, minutesAsleep': USize = 0) =>
        _id = id'
        _minutesAsleep = minutesAsleep'

    new initEmpty() =>
        _id = 0
        _minutesAsleep = 0

    fun ref assignId(id': USize) : None => _id = id'
    fun ref addSleepTime(sleepTime: USize) : None => _minutesAsleep = _minutesAsleep + sleepTime
    fun isZombieGuard() : Bool => _id == 0
    fun id() : USize => _id
    fun minutesAsleep() : USize => _minutesAsleep
    fun string() : String => "#" + _id.string() + " | minutes asleep: " + _minutesAsleep.string()

class SolveDay4

    var _data: Array[String] = []
    let _env: Env
    var _events: Array[Event] = []
    var _guards: Array[Guard] = []

    new create(fileName: String, env: Env) =>
        _env = env
        let caps = recover val FileCaps.>set(FileRead).>set(FileStat) end
        try
            with file = OpenFile(FilePath(_env.root as AmbientAuth, fileName, caps)?) as File
            do
                for line in file.lines() do
                    _data.push(consume line)
                end
            end
        else
            _env.err.print("Could not open " + fileName)
        end

    fun solvePart1() : USize? => 
        var sleepiestGuard: Guard box = _guards.apply(0)?
        for guard in _guards.values() do
            if (guard.minutesAsleep() > sleepiestGuard.minutesAsleep()) then sleepiestGuard = guard end
        end
        sleepiestGuard.id() * doF_ingPonyStuff(sleepiestGuard.id())?._1

    fun solvePart2() : USize? =>
        var ponyGuardTuple: (USize, USize) = (0, 0)
        var ponyGuardId: USize = 0
        for guard in _guards.values() do
            var newPonyGuardTuple: (USize, USize) = doF_ingPonyStuff(guard.id())?
            if newPonyGuardTuple._2 > ponyGuardTuple._2 then
                ponyGuardTuple = newPonyGuardTuple
                ponyGuardId = guard.id()
            end
        end
        ponyGuardId * ponyGuardTuple._1

    fun doF_ingPonyStuff(guardId': USize) : (USize, USize)? =>
        let minutes: Array[USize] = Array[USize].init(0, 60)

        let iterator = _events.values()
        var lastGuard: Guard = Guard.initEmpty()
        var lastSleepPhaseBegin: USize = -1
        while iterator.has_next() do
            let event = iterator.next()?
            if event.description().contains("Guard") then
                if not lastGuard.isZombieGuard() then
                    lastGuard = Guard.initEmpty()
                    lastSleepPhaseBegin = -1
                end
                let guardIdStr: String = event.description().split(" ").apply(1)?
                let guardId: USize = guardIdStr.substring(1, guardIdStr.size().isize()).usize()?
                lastGuard.assignId(guardId)
            elseif event.description() == "falls asleep" then   
                lastSleepPhaseBegin = event.date().min.usize()
            elseif event.description() == "wakes up" then
                if lastGuard.id() == guardId' then
                    for i in Range(lastSleepPhaseBegin, event.date().min.usize()) do
                        minutes.update(i, minutes.apply(i)? + 1)?
                    end
                end
                lastSleepPhaseBegin = -1
            end
        end
        var maxMinute: USize = 0
        var maxMinuteTimes: USize = minutes.apply(0)?
        for (k, v) in minutes.pairs() do
            if v > maxMinuteTimes then
                maxMinute = k
                maxMinuteTimes = v
            end
        end
        (maxMinute, maxMinuteTimes)

    fun printData() : None =>
        for event in _events.values() do
            _env.out.print(event.string())
        end

    fun printGuards() : None =>
        for guard in _guards.values() do
            _env.out.print(guard.string())
        end

    fun ref sort() : None? =>
        let events: Array[Event] = []
        for line in _data.values() do
            let dateTime: String = line.substring(1, 17)
            let date: PosixDate = PosixDate.create()
            date.year = dateTime.substring(0, 4).i32()?
            date.month = dateTime.substring(5, 7).i32()?
            date.day_of_month = dateTime.substring(8, 10).i32()?
            date.hour = dateTime.substring(11, 13).i32()?
            date.min = dateTime.substring(14, 16).i32()?
            let description: String = line.substring(19, line.size().isize())
            let nextIndex: USize = _getNextIndexPosition(date)?
            let newEvent: Event = Event.create(date, description)
            _events.insert(nextIndex, newEvent)?
        end

    fun ref initGuards() : None? =>
        let iterator = _events.values()
        var lastGuard: Guard = Guard.initEmpty()
        var lastSleepPhaseBegin: USize = -1
        while iterator.has_next() do
            let event = iterator.next()?
            if event.description().contains("Guard") then
                if not lastGuard.isZombieGuard() then
                    _updateAndSaveGuard(lastGuard)
                    lastSleepPhaseBegin = -1
                    lastGuard = Guard.initEmpty()
                end
                let guardIdStr: String = event.description().split(" ").apply(1)?
                let guardId: USize = guardIdStr.substring(1, guardIdStr.size().isize()).usize()?
                lastGuard.assignId(guardId)
            elseif event.description() == "falls asleep" then
                lastSleepPhaseBegin = event.date().min.usize()
            elseif event.description() == "wakes up" then
                lastGuard.addSleepTime(event.date().min.usize() - lastSleepPhaseBegin)
                lastSleepPhaseBegin = -1
                if not iterator.has_next() then
                    _updateAndSaveGuard(lastGuard)
                end
            end
        end

    fun ref _updateAndSaveGuard(guard': Guard) : None =>
        var foundGuard: Bool = false
        for guard in _guards.values() do
            if (guard'.id() == guard.id()) then
                foundGuard = true
                guard.addSleepTime(guard'.minutesAsleep())
            end
        end
        if not foundGuard then
            _guards.push(guard')
        end

    fun _getNextIndexPosition(date: PosixDate) : USize? =>
        let iterator = _events.pairs()
        while iterator.has_next() do
            let pair = iterator.next()?
            if date.time() < pair._2.date().time() then
                return pair._1
            end
        end
        _events.size().usize()

actor Main
    new create(env: Env) =>
        let fileName: String = "./aoc_4.input.txt"
        let s: SolveDay4 = SolveDay4(fileName, env)
        try
            s.sort()?
            s.initGuards()?
            env.out.print("Result for Part 1 is: " + s.solvePart1()?.string())
            env.out.print("Result for Part 2 is: " + s.solvePart2()?.string())
        end
