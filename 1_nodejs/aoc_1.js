const fs = require('fs');

class Solve_Day_1 {

    constructor() {
        this._result = 0;
        this._content = fs.readFileSync('./aoc_1.input.txt').toString();
        this._lines = this._content.split(/\r\n|\n/);
        this._found = false;
        this._frequencies = [];
    }

    get result() {
        return this._result;
    }

    solvePart1() {
        this._lines.forEach(line => {
            this._result += this._getNumber(line);
        });
    }

    solvePart2() {
        this._result = 0;
        this._runPart2();
    }

    _runPart2() {
        for (const line of this._lines) {
            this._result += this._getNumber(line);
            if (this._frequencies.includes(this._result)) {
                this._found = true;
                return;
            }
            this._frequencies.push(this._result);
        }
        this._runPart2();
    }

    _getNumber(str) {
        if (str[0] === '+') {
            return Number.parseInt(str.substr(1));
        } else {
            return Number.parseInt(str.substr(1)) * -1;
        }
    }

}

s = new Solve_Day_1();
s.solvePart1();
console.log('Part 1:', s.result);
s.solvePart2();
console.log('Part 2', s.result);
