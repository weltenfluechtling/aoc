<?php

$fn = fopen('./input.txt', 'r');
$content = array();

while (!feof($fn)) {
    $line = fgets($fn);

    $before = substr($line, 5, 1);
    $after = substr($line, 36, 1);

    $before_index = array_search($before, $content);
    $after_index = array_search($after, $content);

    if ($before_index !== FALSE) {
        if ($after_index === FALSE) {
            $content_len = count($content);
            for ($i = $before_index + 1; $i < $content_len; ++$i) {
                if (($after <=> $content[$i]) === -1) {
                    array_splice($content, $i, 0, $after);
                    break;
                } else if ($i === $content_len - 1) {
                    array_push($content, $after);
                    break;
                }
            }
        } elseif ($after_index < $before_index) {
            array_splice($content, $after_index, 1);
            $content_len = count($content);
            $before_index = array_search($before, $content);
            if ($before_index === $content_len - 1) {
                array_push($content, $after);
            } else {
                for ($j = $before_index + 1; $j < $content_len; ++$j) {
                    if (($after <=> $content[$j]) === -1) {
                        array_splice($content, $i, 0, $after);
                        break;
                    } else if ($i === $content_len - 1) {
                        array_push($content, $after);
                        break;
                    }
                }
            }
        }
    } elseif ($after_index !== FALSE) {
        if ($after_index === 0) {
            array_splice($content, 0, 0, $before);
        } else {
            for ($i = $after_index - 1; $i >= 0; --$i) {
                if (($before <=> $content[$i]) === -1) {
                    array_splice($content, $i, 0, $before);
                    break;
                } else if ($i === 0) {
                    array_splice($content, $i, 0, $before);
                    break;
                }
            }
        }
    }

    // Neither $before nor $after have been found in the array.
    if ($before_index === FALSE && $after_index === FALSE) {
        $content_len = count($content);
        if ($content_len === 0) {
            array_push($content, $before, $after);
        } else {
            for ($i = 0; $i < $content_len; ++$i) {
                if ($i === $content_len - 1) {
                    array_push($content, $before, $after);
                    break;
                }
                if (($before <=> $content[$i]) === -1) {
                    array_splice($content, $i, 0, $before);
                    for ($j = $i + 1; $j < count($content); ++$j) {
                        if (($after <=> $content[$j]) === -1) {
                            array_splice($content, $j, 0, $after);
                            break;
                        } else if ($j === count($content) - 1) {
                            array_push($content, $after);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}

echo implode("", $content) . "\n";